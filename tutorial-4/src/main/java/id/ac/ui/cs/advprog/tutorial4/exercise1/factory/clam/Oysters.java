package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class Oysters implements Clams {

    public String toString() {
        return "Fresh Oysters from South Carolina";
    }
}
