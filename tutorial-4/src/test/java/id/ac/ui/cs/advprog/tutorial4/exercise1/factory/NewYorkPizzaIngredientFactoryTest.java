package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {
    private NewYorkPizzaIngredientFactory newYorkPizzaIngredientFactory;
    private ReggianoCheese cheese;
    private FreshClams clam;
    private ThinCrustDough dough;
    private MarinaraSauce sauce;
    private Veggies[] veggies = {
        new Garlic(),
        new Onion(),
        new Mushroom(),
        new RedPepper()
    };

    @Before
    public void setUp() {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
        cheese = new ReggianoCheese();
        clam = new FreshClams();
        dough = new ThinCrustDough();
        sauce = new MarinaraSauce();
    }

    @Test
    public void testCheeseIngredient() {
        assertEquals(newYorkPizzaIngredientFactory.createCheese().toString(), cheese.toString());
    }

    @Test
    public void testClamIngredient() {
        assertEquals(newYorkPizzaIngredientFactory.createClam().toString(), clam.toString());
    }

    @Test
    public void testDoughIngredient() {
        assertEquals(newYorkPizzaIngredientFactory.createDough().toString(), dough.toString());
    }

    @Test
    public void testSauceIngredient() {
        assertEquals(newYorkPizzaIngredientFactory.createSauce().toString(), sauce.toString());
    }

    @Test
    public void testVeggiesIngredient() {
        Veggies[] veggiesComparation = newYorkPizzaIngredientFactory.createVeggies();
        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].toString(), veggiesComparation[i].toString());
        }
    }
}