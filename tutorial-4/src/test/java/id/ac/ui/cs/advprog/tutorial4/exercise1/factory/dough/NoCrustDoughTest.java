package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;

import org.junit.Before;
import org.junit.Test;

public class NoCrustDoughTest {
    private NoCrustDough noCrustDough;

    @Before
    public void setUp() {
        noCrustDough = new NoCrustDough();
    }

    @Test
    public void testNoCrustDough() {
        assertEquals("NoCrust style no crust dough", noCrustDough.toString());
    }
}