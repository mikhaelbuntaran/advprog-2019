package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Oysters;

import org.junit.Before;
import org.junit.Test;

public class OystersTest {
    private Oysters oysters;

    @Before
    public void setUp() {
        oysters = new Oysters();
    }

    @Test
    public void testOysters() {
        assertEquals("Fresh Oysters from South Carolina", oysters.toString());
    }
}