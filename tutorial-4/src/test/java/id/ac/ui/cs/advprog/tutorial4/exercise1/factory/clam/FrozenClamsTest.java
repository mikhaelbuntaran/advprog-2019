package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        frozenClams = new FrozenClams();
    }

    @Test
    public void testFrozenClams() {
        assertEquals("Frozen Clams from Chesapeake Bay", frozenClams.toString());
    }
}