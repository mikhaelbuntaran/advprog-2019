package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BarbequeSauce;

import org.junit.Before;
import org.junit.Test;

public class BarbequeSauceTest {
    private BarbequeSauce bbqSauce;

    @Before
    public void setUp() {
        bbqSauce = new BarbequeSauce();
    }

    @Test
    public void testBarbequeSauce() {
        assertEquals("BBQ Sauce", bbqSauce.toString());
    }
}