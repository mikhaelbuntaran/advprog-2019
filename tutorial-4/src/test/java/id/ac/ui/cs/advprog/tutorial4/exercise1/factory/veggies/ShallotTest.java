package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Shallot;

import org.junit.Before;
import org.junit.Test;

public class ShallotTest {
    private Shallot shallot;

    @Before
    public void setUp() {
        shallot = new Shallot();
    }

    @Test
    public void testShallot() {
        assertEquals("Shallot", shallot.toString());
    }
}