package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;
    private Pizza cheesePizza;
    private Pizza clamPizza;
    private Pizza veggiePizza;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
        cheesePizza = depokPizzaStore.orderPizza("cheese");
        clamPizza = depokPizzaStore.orderPizza("clam");
        veggiePizza = depokPizzaStore.orderPizza("veggie");
    }

    @Test
    public void testDepokCheesePizza() {
        assertEquals("Depok Style Cheese Pizza", cheesePizza.getName());
    }

    @Test
    public void testDepokClamPizza() {
        assertEquals("Depok Style Clam Pizza", clamPizza.getName());
    }

    @Test
    public void testDepokVeggiePizza() {
        assertEquals("Depok Style Veggie Pizza", veggiePizza.getName());
    }
}
