package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore nyPizzaStore;
    private Pizza cheesePizza;
    private Pizza clamPizza;
    private Pizza veggiePizza;

    @Before
    public void setUp() {
        nyPizzaStore = new NewYorkPizzaStore();
        cheesePizza = nyPizzaStore.orderPizza("cheese");
        clamPizza = nyPizzaStore.orderPizza("clam");
        veggiePizza = nyPizzaStore.orderPizza("veggie");
    }

    @Test
    public void testNewYorkCheesePizza() {
        assertEquals("New York Style Cheese Pizza", cheesePizza.getName());
    }

    @Test
    public void testNewYorkClamPizza() {
        assertEquals("New York Style Clam Pizza", clamPizza.getName());
    }

    @Test
    public void testNewYorkVeggiePizza() {
        assertEquals("New York Style Veggie Pizza", veggiePizza.getName());
    }
}
