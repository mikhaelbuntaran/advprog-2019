package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;

import org.junit.Before;
import org.junit.Test;

public class RedPepperTest {
    private RedPepper redPepper;

    @Before
    public void setUp() {
        redPepper = new RedPepper();
    }

    @Test
    public void testRedPepper() {
        assertEquals("Red Pepper", redPepper.toString());
    }
}