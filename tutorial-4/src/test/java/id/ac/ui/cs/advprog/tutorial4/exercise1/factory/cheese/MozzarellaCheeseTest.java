package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarellaCheese;

    @Before
    public void setUp() {
        mozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void testMozzarellaCheese() {
        assertEquals("Shredded Mozzarella", mozzarellaCheese.toString());
    }
}