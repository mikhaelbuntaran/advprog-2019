package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Oysters;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BarbequeSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Shallot;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;
    private CheddarCheese cheese;
    private Oysters clam;
    private NoCrustDough dough;
    private BarbequeSauce sauce;
    private Veggies[] veggies = {
        new BlackOlives(),
        new Eggplant(),
        new Garlic(),
        new Mushroom(),
        new Onion(),
        new Shallot(),
        new Spinach()
    };

    @Before
    public void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
        cheese = new CheddarCheese();
        clam = new Oysters();
        dough = new NoCrustDough();
        sauce = new BarbequeSauce();
    }

    @Test
    public void testCheeseIngredient() {
        assertEquals(depokPizzaIngredientFactory.createCheese().toString(), cheese.toString());
    }

    @Test
    public void testClamIngredient() {
        assertEquals(depokPizzaIngredientFactory.createClam().toString(), clam.toString());
    }

    @Test
    public void testDoughIngredient() {
        assertEquals(depokPizzaIngredientFactory.createDough().toString(), dough.toString());
    }

    @Test
    public void testSauceIngredient() {
        assertEquals(depokPizzaIngredientFactory.createSauce().toString(), sauce.toString());
    }

    @Test
    public void testVeggiesIngredient() {
        Veggies[] veggiesComparation = depokPizzaIngredientFactory.createVeggies();
        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].toString(), veggiesComparation[i].toString());
        }
    }
}