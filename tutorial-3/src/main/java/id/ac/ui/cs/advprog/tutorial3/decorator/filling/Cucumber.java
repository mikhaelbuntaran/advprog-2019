package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        //TODO Implement
        this.food = food;
        this.description = "cucumber";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription() + ", adding " + this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return .40 + food.cost();
    }
}
