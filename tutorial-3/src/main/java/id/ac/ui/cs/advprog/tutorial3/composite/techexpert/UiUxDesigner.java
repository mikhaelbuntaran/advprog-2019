package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        if (salary < 90000.00) {
            throw new IllegalArgumentException(
                    "Invalid salary: " + salary + " (min. 90000.00)");
        }
        this.salary = salary;
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
