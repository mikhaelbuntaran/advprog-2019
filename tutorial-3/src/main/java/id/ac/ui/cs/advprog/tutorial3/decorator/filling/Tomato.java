package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        //TODO Implement
        this.food = food;
        this.description = "tomato";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription() + ", adding " + this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return .50 + food.cost();
    }
}
