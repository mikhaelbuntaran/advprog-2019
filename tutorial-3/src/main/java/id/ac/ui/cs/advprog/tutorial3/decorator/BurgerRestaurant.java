package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class BurgerRestaurant {
    public static void main(String[] args) {
    	Food myFavouriteBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();
    	myFavouriteBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(myFavouriteBurger);
    	myFavouriteBurger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(myFavouriteBurger);
    	myFavouriteBurger = FillingDecorator.CHEESE.addFillingToBread(myFavouriteBurger);
        myFavouriteBurger = FillingDecorator.TOMATO.addFillingToBread(myFavouriteBurger);
    	myFavouriteBurger = FillingDecorator.LETTUCE.addFillingToBread(myFavouriteBurger);
        System.out.println("This is my favourite burger:");
    	System.out.println(myFavouriteBurger.getDescription()
                + ": $" + myFavouriteBurger.cost()); // cost should be $11.95
    }
}
