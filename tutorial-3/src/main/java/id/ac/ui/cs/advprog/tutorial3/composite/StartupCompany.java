package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;

public class StartupCompany {
	public static void main(String[] args) {
		Company strawHatPirate = new Company();

	    Employees captain = new Ceo("Monkey D. Luffy", 1500000.00);
		Employees swordman = new Cto("Zoro", 800000.00);
		Employees shooter = new BackendProgrammer("Usopp", 500000.00);
		Employees navigator = new FrontendProgrammer("Nami", 200000.00);
		Employees cook = new NetworkExpert("Vinsmoke Sanji", 500000.00);
		// any other nakama....

        strawHatPirate.addEmployee(captain);
        strawHatPirate.addEmployee(swordman);
        strawHatPirate.addEmployee(shooter);
        strawHatPirate.addEmployee(navigator);
        strawHatPirate.addEmployee(cook);

        System.out.println("Mugiwara's total bounty:");
        System.out.println(strawHatPirate.getNetSalaries()); // expected output: 3,5m berry
    }
}